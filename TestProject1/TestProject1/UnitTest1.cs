﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Reflection;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;

namespace GoogleCloudPricingCalculator
{
    public class WebDriverManager
    {
        public static IWebDriver CreateDriver()
        {
            return new ChromeDriver();
        }

        public static void QuitDriver(IWebDriver driver)
        {
            driver.Quit();
        }
    }

    public class GoogleCloudPlatformHomePage
    {
        private readonly IWebDriver _driver;

        public GoogleCloudPlatformHomePage(IWebDriver driver)
        {
            _driver = driver;
        }

        public void OpenPage()
        {
            _driver.Navigate().GoToUrl("https://cloud.google.com/");
        }

        public void SearchForPricingCalculator()
        {
            _ = new WebDriverWait(_driver, TimeSpan.FromSeconds(100));
            IWebElement searchInput = _driver.FindElement(By.XPath("//*[@id=\"kO001e\"]/div[2]/div[1]/div/div[2]/div[2]/div[1]/div/div"));
            searchInput.Click();
            _ = _driver.FindElement(By.XPath("//*[@id=\"i31\"]"));
            searchInput.SendKeys("Google Cloud Platform Pricing Calculator");
            searchInput.SendKeys(Keys.Enter);
        }
    }

    public class GoogleCloudPricingCalculatorPage
    {
        private readonly IWebDriver _driver;

        public GoogleCloudPricingCalculatorPage(IWebDriver driver)
        {
            _driver = driver;
        }

        public void ClickOnGoogleCloudPricingCalculator()
        {
            var pricingCalculatorLink = _driver.FindElement(By.XPath("//*[@id=\"yDmH0d\"]/c-wiz[1]/div/div/div/div/div/div[3]/c-wiz/div[1]/div[1]/div/div[1]/a"));
            pricingCalculatorLink.Click();
        }

        public void FillComputeEngineForm(ComputeEngineFormData data)
        {
            var computeEngineButton = _driver.FindElement(By.XPath("//*[@id=\"yDmH0d\"]/div[5]/div[2]/div/div/div/div[2]/div/div/div[1]/div/div/div"));
            computeEngineButton.Click();

            var instances = _driver.FindElement(By.Name("quantity"));
            instances.SendKeys(data.InstanceCount.ToString());

            var operatingSystem = _driver.FindElement(By.Name("os"));
            operatingSystem.Click();
            _driver.FindElement(By.XPath($"//md-option[@value='{data.OperatingSystem}']"))
                   .Click();

            var provisioningModel = _driver.FindElement(By.Name("provisioningModel"));
            provisioningModel.Click();
            _driver.FindElement(By.XPath($"//md-option[@value='{data.ProvisioningModel}']"))
                   .Click();

            var machineFamily = _driver.FindElement(By.Name("family"));
            machineFamily.Click();
            _driver.FindElement(By.XPath($"//md-option[@value='{data.MachineFamily}']"))
                   .Click();

            var series = _driver.FindElement(By.Name("series"));
            series.Click();
            _driver.FindElement(By.XPath($"//md-option[contains(text(), '{data.Series}')]"))
                   .Click();

            var machineType = _driver.FindElement(By.Name("instanceType"));
            machineType.Click();
            _driver.FindElement(By.XPath($"//md-option[contains(text(), '{data.MachineType}')]"))
                   .Click();

            if (data.AddGPU)
            {
                var addGpuButton = _driver.FindElement(By.XPath("//md-checkbox[@aria-label='Add GPUs']"));
                addGpuButton.Click();

                var gpuType = _driver.FindElement(By.Name("gpuType"));
                gpuType.Click();
                _driver.FindElement(By.XPath($"//md-option[contains(text(), '{data.GPUType}')]"))
                       .Click();

                var gpuCount = _driver.FindElement(By.Name("gpuCount"));
                gpuCount.SendKeys(data.GPUCount.ToString());
            }

            var localSsd = _driver.FindElement(By.Name("localSsd"));
            localSsd.Click();
            _driver.FindElement(By.XPath($"//md-option[contains(text(), '{data.LocalSSD}')]"))
                   .Click();

            var datacenter = _driver.FindElement(By.Name("location"));
            datacenter.Click();
            _driver.FindElement(By.XPath($"//md-option[contains(text(), '{data.DatacenterLocation}')]"))
                   .Click();

            var committedUsage = _driver.FindElement(By.Name("commitmentTerm"));
            committedUsage.Click();
            _driver.FindElement(By.XPath($"//md-option[contains(text(), '{data.CommittedUsage}')]"))
                   .Click();

            var addToEstimateButton = _driver.FindElement(By.XPath("//button[contains(text(), 'Add to Estimate')]"));
            addToEstimateButton.Click();
        }

        public string GetEstimatedCost()
        {
            var totalCostElement = _driver.FindElement(By.XPath("//div[@class='cpc-cart-total']/h2"));
            return totalCostElement.Text;
        }

        public void ShareEstimate()
        {
            var shareButton = _driver.FindElement(By.XPath("//button[contains(text(), 'Share')]"));
            shareButton.Click();
        }

        public void OpenEstimateSummary()
        {
            var openSummaryButton = _driver.FindElement(By.XPath("//button[contains(text(), 'Open estimate summary')]"));
            openSummaryButton.Click();
        }
    }

    public class ComputeEngineFormData
    {
        public int InstanceCount { get; set; }
        public string OperatingSystem { get; set; }
        public string ProvisioningModel { get; set; }
        public string MachineFamily { get; set; }
        public string Series { get; set; }
        public string MachineType { get; set; }
        public bool AddGPU { get; set; }
        public string GPUType { get; set; }
        public int GPUCount { get; set; }
        public string LocalSSD { get; set; }
        public string DatacenterLocation { get; set; }
        public string CommittedUsage { get; set; }
    }

    public static class ScreenshotUtility
    {
        public static void CaptureScreenshot(IWebDriver driver, string testName)
        {
            var timestamp = DateTime.Now.ToString("yyyy-MM-dd-HH-mm-ss");
            var screenshot = ((ITakesScreenshot)driver).GetScreenshot();
            using (MemoryStream memoryStream = new MemoryStream(screenshot.AsByteArray))
            {
                using (Bitmap bitmap = new Bitmap(memoryStream))
                {
                    var filePath = Path.Combine(Directory.GetCurrentDirectory(), $"screenshot_{testName}_{timestamp}.png");
                    bitmap.Save(filePath, ImageFormat.Png);
                }
            }
        }
    }

    [TestFixture]
    public class GoogleCloudPricingCalculatorTests : IDisposable
    {
        private IWebDriver _driver;
        private GoogleCloudPlatformHomePage _homePage;
        private GoogleCloudPricingCalculatorPage _calculatorPage;

        [SetUp]
        public void Setup()
        {
            _driver = WebDriverManager.CreateDriver();
            _driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);

            _homePage = new GoogleCloudPlatformHomePage(_driver);
            _calculatorPage = new GoogleCloudPricingCalculatorPage(_driver);
        }

        [Test]
        public void TestPricingCalculator()
        {
            _homePage.OpenPage();
            _homePage.SearchForPricingCalculator();

            _calculatorPage.ClickOnGoogleCloudPricingCalculator();

            var environment = "development";
            var testData = LoadTestData(environment);

            _calculatorPage.FillComputeEngineForm(testData);
            var estimatedCost = _calculatorPage.GetEstimatedCost();

            Console.WriteLine($"Estimated Cost: {estimatedCost}");

            _calculatorPage.ShareEstimate();
            _calculatorPage.OpenEstimateSummary();
        }

        private ComputeEngineFormData LoadTestData(string environment)
        {
            var testData = new ComputeEngineFormData();

            var testDataFile = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), "testData.properties");
            var lines = File.ReadAllLines(testDataFile);

            for (int i = 0; i < lines.Length; i++)
            {
                var line = lines[i];
                if (line.StartsWith($"[{environment}]"))
                {
                    while ((line = lines[++i]) != "")
                    {
                        var parts = line.Split('=');
                        var propertyName = parts[0].Trim();
                        var propertyValue = parts[1].Trim();

                        var property = testData.GetType().GetProperty(propertyName);
                        if (property != null)
                        {
                            if (property.PropertyType == typeof(int))
                            {
                                property.SetValue(testData, int.Parse(propertyValue));
                            }
                            else if (property.PropertyType == typeof(bool))
                            {
                                property.SetValue(testData, bool.Parse(propertyValue));
                            }
                            else
                            {
                                property.SetValue(testData, propertyValue);
                            }
                        }
                    }
                    break;
                }
            }

            return testData;
        }

        [TearDown]
        public void Teardown()
        {
            Dispose();
        }

        public void Dispose()
        {
            _driver.Quit();
        }
    }
}
